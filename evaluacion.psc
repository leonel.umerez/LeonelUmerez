Algoritmo sin_titulo
	Definir empleado,sueldo,cat,cat1,pagocat1,sueldoscat1,cat2,pagocat2,sueldoscat2,cat3,pagocat3,sueldoscat3,antig,extra,sueldostotal,sueldosprom,sueldomax,catsueldomax como entero;
	empleado=0;
	sueldo=0;
	cat=0;
	cat1=0;
	pagocat1=0;
	sueldoscat1=0;
	cat2=0;
	pagocat2=0;
	sueldoscat2=0;
	cat3=0;
	pagocat3=0;
	sueldoscat3=0;
	antig=0;
	extra=0;
	sueldostotal=0;
	sueldosprom=0;
	sueldomax=0;
	catsueldomax=0;
	Para empleado<-1 Hasta 50 Hacer
		Escribir 'Cite antig�edad (en a�os) del trabajador:';
		Leer antig;
		extra <- 100*antig;
		Repetir
			Escribir 'Cite categor�a del empleado:';
			Leer cat;
		Mientras Que cat!=1 && cat!=2 && cat!=3
		Segun cat  Hacer
			1:
				cat1 <- cat1+1;
				pagocat1 = 1500;
				sueldo <- pagocat1+extra;
				sueldoscat1 <- sueldoscat1+sueldo;
				Si sueldo>sueldomax Entonces
					sueldomax <- sueldo;
					catsueldomax <- 1;
				FinSi
			2:
				cat2 <- cat2+1;
				pagocat2 = 1700;
				sueldo <- pagocat2+extra;
				sueldoscat2 <- sueldoscat2+sueldo;
				Si sueldo>sueldomax Entonces
					sueldomax <- sueldo;
					catsueldomax <- 2;
				FinSi
			De Otro Modo:
				cat3 <- cat3+1;
				pagocat3 = 2000;
				sueldo <- pagocat3+extra;
				sueldoscat3 <- sueldoscat3+sueldo;
				Si sueldo>sueldomax Entonces
					sueldomax <- sueldo;
					catsueldomax <- 3;
				FinSi
		FinSegun
	FinPara
	sueldostotal <- sueldoscat1+sueldoscat2+sueldoscat3;
	sueldosprom <- sueldostotal/empleado;
	Escribir "En la categor�a 1 hay " cat1 " empleados; en la 2 hay " cat2 ", mientras que en la 3 son " cat3 " empleados.";
	Escribir "Los sueldos de la categor�a 1 hacen un total de $" sueldoscat1 ". Los sueldos de la categor�a dos en su conjunto totalizan $" sueldoscat2 ", mientras que los de la 3 suman $" sueldoscat3 ".";
	Escribir "El sueldo promedio  es de $" sueldosprom ".";
	Escribir "El m�ximo sueldo registrado es de $" sueldomax ", y se registra en la categor�a " catsueldomax ".";
FinAlgoritmo

