/* Este codigo ha sido generado por el modulo psexport 20160506-w32 de PSeInt.
Es posible que el codigo generado no sea completamente correcto. Si encuentra
errores por favor reportelos en el foro (http://pseint.sourceforge.net). */

// En java, el nombre de un archivo fuente debe coincidir con el nombre de la clase que contiene,
// por lo que este archivo deber�a llamarse "SIN_TITULO.java."
import java.util.Scanner;
import java.io.*;

public class evaluacion 
{
	public static void main(String args[]) throws IOException {
		BufferedReader bufEntrada = new BufferedReader(new InputStreamReader(System.in));
		int antig;
		int cat;
		int cat1;
		int cat2;
		int cat3;
		int catsueldomax;
		int empleado;
		int extra;
		int pagocat1;
		int pagocat2;
		int pagocat3;
		int sueldo;
		int sueldomax;
		int sueldoscat1;
		int sueldoscat2;
		int sueldoscat3;
		int sueldosprom;
		int sueldostotal;
		empleado = 0;
		sueldo = 0;
		cat = 0;
		cat1 = 0;
		pagocat1 = 0;
		sueldoscat1 = 0;
		cat2 = 0;
		pagocat2 = 0;
		sueldoscat2 = 0;
		cat3 = 0;
		pagocat3 = 0;
		sueldoscat3 = 0;
		antig = 0;
		extra = 0;
		sueldostotal = 0;
		sueldosprom = 0;
		sueldomax = 0;
		catsueldomax = 0;
	for (empleado=1;empleado<=5;empleado++) 
		{
			System.out.println("Cite antigüedad (en años) del trabajador:");
			antig = Integer.parseInt(bufEntrada.readLine());
			extra = 100*antig;
			do 
			{
				System.out.println("Cite categoría del empleado:");
				cat = Integer.parseInt(bufEntrada.readLine());
			} 
			while (cat!=1 && cat!=2 && cat!=3);

			switch (cat) 
			{
			case 1:
				cat1 = cat1+1;
				pagocat1 = 1500;
				sueldo = pagocat1+extra;
				sueldoscat1 = sueldoscat1+sueldo;
				if (sueldo>sueldomax) 
				{
					sueldomax = sueldo;
					catsueldomax = 1;
				}
				break;
			case 2:
				cat2 = cat2+1;
				pagocat2 = 1700;
				sueldo = pagocat2+extra;
				sueldoscat2 = sueldoscat2+sueldo;
				if (sueldo>sueldomax) 
				{
					sueldomax = sueldo;
					catsueldomax = 2;
				}
				break;
			default:
				cat3 = cat3+1;
				pagocat3 = 2000;
				sueldo = pagocat3+extra;
				sueldoscat3 = sueldoscat3+sueldo;
				if (sueldo>sueldomax) 
				{
					sueldomax = sueldo;
					catsueldomax = 3;
				}
			}
		}
		sueldostotal = sueldoscat1+sueldoscat2+sueldoscat3;
		sueldosprom = sueldostotal/empleado;
		System.out.println("En la categoría 1 hay "+cat1+" empleados; en la 2 hay "+cat2+", mientras que en la 3 son "+cat3+" empleados.");
		System.out.println("Los sueldos de la categoría 1 hacen un total de $"+sueldoscat1+". Los sueldos de la categoría dos en su conjunto totalizan $"+sueldoscat2+", mientras que los de la 3 suman $"+sueldoscat3+".");
		System.out.println("El sueldo promedio  es de $"+sueldosprom+".");
		System.out.println("El máximo sueldo registrado es de $"+sueldomax+", y se registra en la categoría "+catsueldomax+".");
	}


}

