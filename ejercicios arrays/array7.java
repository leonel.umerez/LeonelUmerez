
/**
 7) Dados los sueldos y edades de N empleados da una empresa, Se pide cargarlos en vectores y luego imprimir:
a.  sueldo promedio
b.  sueldo promedio de los empleados que tengan entre 18 y 20 años
c.  edad promedio
d.  cantidad de empleados con sueldo mayor al sueldo promedio.
e.  cantidad de empleados con edad menor a la edad promedio.

 */
public class array7
{
    public static void main(String[] args){
     
        int empleados= (int)Math.floor(Math.random()*25);
        
        int sueldo []= new int[empleados];
        
        int edad []= new int [empleados];
        
        int sumasueldos=0;
        
        int sumaedad=0;
        
        int empljoven=0; 
        
        int sueldojoven=0;
        
        for(int i=0; i<empleados; i++){
         
            sueldo[i]=(int)Math.floor(Math.random()*(10000+5000)+10000);
            edad[i]=(int)Math.floor(Math.random()*(35+10)+18);
            
            sumasueldos = sumasueldos+sueldo[i];
            sumaedad = sumaedad+edad[i];
            
            if(edad[i]>=18 && edad[i]<=20){
                
                empljoven++;
                
                sueldojoven = sueldojoven+sueldo[i];
                
            }
            /*System.out.println("EL sueldo es de " + sueldo[i]);
            System.out.println("la edad es de " + edad[i]);*/
        }
              
        double sueldoprom = sumasueldos/empleados;
        
        System.out.println("El sueldo promedio es de $" + sueldoprom);
        
        double promsueldojoven = sueldojoven/empljoven;
        
        System.out.println("El sueldo promedio de los jovenes entre 18 y 20 años es de $" + promsueldojoven);
        
        double promedad = sumaedad/empleados;
        
        System.out.println("La edad promedio es de " + promedad);
        
        int emplMasSueldo=0;
        
        int emplMasJoven=0;
        
        for(int i=0; i<empleados; i++){
            
            if(sueldo[i]>sueldoprom){
                
                emplMasSueldo++;
            }
            
            if(edad[i]<promedad){
                
                emplMasJoven++;
                
            }
        }
        
        System.out.println("La cantidad de empleados que perciben mayor salario que el promedio es de " + emplMasSueldo);
        System.out.println("La cantidad de empleados con menor edad al promedio de la plantilla es de " + emplMasJoven);
    }
}
