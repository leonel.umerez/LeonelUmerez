
/**
 8) Dadas N notas y edades de alumnos, se pide  cargarlas en vectores y luego imprimir:
a.  cantidad de alumnos aprobados 
b.  cantidad de alumnos aplazados 
c.  edad promedio
d.  nota promedio de los alumnos mayores que 15.
 */
public class array8
{
    public static void main(String[] args){
        
        int n= (int)Math.floor(Math.random()*100);
        
        int nota []= new int [n];
        
        int edad []= new int [n];
        
        int aprobados=0;
        int aplazados=0;
        int sumaedad=0;
        int sumanotas=0;
        int alumnosMayores=0;
        int notaAlumnosMayores=0;
        
        for(int i=0; i<n; i++){
            
            nota[i]=(int)Math.floor(Math.random()*10);
            edad[i]=(int)Math.floor(Math.random()*(6+6)+6);
            
            sumaedad=sumaedad+edad[i];
            sumanotas=sumanotas+nota[i];
            
            if(nota[i]>7){
                
                aprobados++;
            
            }else{

                aplazados++;
                
            }
            
            if(edad[i]>14){
                
                alumnosMayores++;
                notaAlumnosMayores=notaAlumnosMayores+nota[i];
            }
            
            /*System.out.println("edad " + edad[i] + " , nota " + nota[i]);*/
        } 
        
        double promedad= sumaedad/n;
        double promMayor= notaAlumnosMayores/alumnosMayores;
        
        System.out.println("La cantidad de alumnos aprobados es de " + aprobados);
        System.out.println("La de aplazados: " + aplazados);
        System.out.println("La edad promedio es de " + promedad);
        System.out.println("El promedio de nota en los alumnos mayores de 15 es de " + promMayor);
            
        }
        
}