/*3)	Cargar un vector de N elementos e imprimir
a.	Cantidad de positivos.
b.	Cantidad de negativos. .
c.	Cantidad de elementos menores que 25.
*/

public class array3
{
    public static void main(String[] args){
        
        int n= (int)Math.floor(Math.random()*100);
        
        int arr[]= new int [n];
        
        int men=0;
        int pos=0;
        int neg=0;
        
        for(int i=0; i<arr.length; i++){
            
            arr[i]=(int)Math.floor(Math.random()*(100+100)-100);
            if(arr[i]>=0){
                
                pos++;
                
            }
            else{
              
                neg++;
                
            }
            
            if(arr[i]<25){
                
                men++;
                
            }
            
            System.out.println(arr[i]);
        }        
        
        System.out.println("La cantidad de elementos positivos es de " + pos + " la de elementos negativos es de " + neg + " mientras que los menores a 25 son" + men + ".");
    }
}
