
/**4)   Dados 20 números, cargarlos en un vector y hallar: 
a.  la suma de los elementos
b.  la cantidad de elementos del vector iguales a 1.
 */
public class array4
{
    public static void main(String[] args){
     
        int arr []= new int [20];
        
        int suma = 0;
        int cont = 0;
        
        for(int i=0; i<arr.length; i++){
            
            arr[i]=(int)Math.floor(Math.random()*100);
            
            suma=suma+arr[i];
            
            if(arr[i]<2 && arr[i]>0){
             
                cont++;
                
            }
            
            System.out.println(arr[i]);
        }
        
        
        System.out.println("La suma de los elementos en la matriz es: " + suma + ". Y la cantidad de elementos iguales a 1 son: " + cont);
        
        
    }
}
