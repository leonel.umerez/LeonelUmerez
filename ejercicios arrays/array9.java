
/**
9)  Se  tiene un vector cargado con 100 números. Se pide hallar la cantidad de positivos, negativos y ceros.
 */
public class array9
{
    public static void main (String[] args){
     
        int n []= new int [100];
        
        int pos=0;
        int neg=0;
        int neu=0;
        
        for(int i=0; i<n.length; i++){
         
            n[i]=(int)Math.floor(Math.random()*(100+100)-100);
            
            if(n[i]>0){
             
                pos++;
            }else{
                
                if(n[i]<0){
                
                neg++;
            }
            else{
                
                neu++;
            }
        }
            System.out.println(n[i]);
        }
        
        System.out.println("En la matriz se encuentran " + pos + " números positivos; " + neg + " números negativos, y " + neu + " vez/veces se repite el 0");
    }
}
