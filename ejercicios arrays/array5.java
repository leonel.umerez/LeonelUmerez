
/**
 5) Cargar un vector de N elementos, .y luego imprimir:   
a.  las componentes pares       
b.  la suma de las componentes.     
c.  el promedio de las componentes.         
d.  porcentaje de positivos.
*/
public class array5
{
    public static void main(String[] args){
     
        int n= (int)Math.floor(Math.random()*50);
        
        int arr[]= new int [n];
        
        int suma = 0;
        int pos = 0;
        
        for(int i=0; i<arr.length; i++){
            
            arr[i]=(int)Math.floor(Math.random()*(100+100)-100);
            
            suma=suma+arr[i];
            
            if(arr[i]>0){
             
                pos++;
                
            }
            
        }
        
        System.out.println("Los componentes pares son los siguientes: ");
        
        for(int i=0; i<arr.length; i+=2){
           
            System.out.println(arr[i]);
            
        }
        
        System.out.println("La suma de los componentes es de: " + suma + ".");
        
        int prom= suma/n;
        
        System.out.println("El promedio de los componentes es de: " + prom + ".");
        
        double porcpos = (pos*100)/n;
        
        System.out.println("El porcentaje de componentes positivos es de: " + porcpos + ".");
    }
}
