
/**
11) Dado un vector de 30 elementos, imprimir el valor máximo
 */
public class array11
{
    public static void main(String [] args){
     
        int n []=new int [30];
        
        int max=0;
        
        for(int i=0; i<n.length; i++){
         
            n[i]=(int)Math.floor(Math.random()*100);
            
            if(n[i]>max){
                
                max=n[i];
        }
        
     }
     
     System.out.println("El valor màximo del array es " + max);
}

}
