
/**
14) Se tienen cargados en un vector los tiempos de clasificación 
de 60 autos. Los autos se identifican con números correlativos del 1 al 60. 
Se pide imprimir:
a.  Nro. de auto que clasificó primero.
b.  Ultimo tiempo de clasificación.

 */
public class array14
{
    public static void main(String [] args){
     
        int n=60;
        
        float tiempo []= new float [n];
        
        float mejor=120;
        float menor=0;
        
        int puntero=0;
        int ultimo=0;
        
        for(int i=0; i<n; i++){
            
            tiempo[i]=(float)(Math.random()*(25+25)+70);
            
            if(tiempo[i]<mejor){
             
                mejor=tiempo[i];
                puntero=i;
            }
            
            if(tiempo[i]>menor){
                
                menor=tiempo[i];
                ultimo=i;
            }
        }
        
        System.out.println("El auto mejor clasificado fue el " + (puntero+1) + " con un tiempo de " + mejor);
        System.out.println("El auto peor clasificado fue el " + (ultimo+1) + " con un tiempo de " + menor);
    }
}
