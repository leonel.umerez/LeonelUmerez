
/**
18)Dado el vector del ej. 14, ordenarlo en forma ascendente. Se deberá imprimir de la siguiente forma:
			Nro. de auto	Tiempo 
				57	2:50 min 				
                                23	2:56 min 				
                                O1 	3:09 min 				
                                14	3:19 min 		etc.
 */
public class array18
{
    public static void main(String [] args){
     
        int n=60;
        
        float tiempo []= new float [n];
        int auto []= new int [n];
        
        float mejor=120;
        float menor=0;
        
        int controlador=1;
        float temp=0;
        int tempauto=0;
        
        
        for(int i=0; i<n; i++){
            
            tiempo[i]=(float)(Math.random()*(10+10)+100);
            auto[i]=i+1;
            
            if(i>n-2){
                
                for(int j=0; controlador>0; j++){
                    
                    controlador=0;
                    
                    for(int z=0; z<n-1; z++){
                        
                        if(tiempo[z]>tiempo[z+1]){
                            
                            temp=tiempo[z+1];
                            tiempo[z+1]=tiempo[z];
                            tiempo[z]=temp;
                            
                            tempauto=auto[z+1];
                            auto[z+1]=auto[z];
                            auto[z]=tempauto;
                            
                            controlador=1;
                        }
                    }
                }
            }
        }
        
        System.out.println("Estos son los resultados de la prueba:");
        System.out.println("Nro de auto     Tiempo");
        
        for(int i=0; i<n; i++){
            
            System.out.println("     " + auto[i] + "         " + tiempo[i]);
            
        }
    }
}
