
/**
12) Dado un vector de N elementos, imprimir el valor mínimo.
 */
public class array12
{
    public static void main(String[] args){
     
        int n=(int)Math.floor(Math.random()*50);
        
        int arr []= new int [n];
        
        int min=1000000;
        
        for(int i=0; i<n; i++){
         
            arr[i]=(int)Math.floor(Math.random()*(100+100)-100);
            
            if(arr[i]<min){
             
                min=arr[i];
            }
            
            System.out.println(arr[i]);
        }
        
        System.out.println("El menor valor del array es " + min);
    }
}
