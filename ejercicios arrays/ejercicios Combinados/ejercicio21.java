package ejerciciosCombinadoss;

/**
21)   Una empresa tiene 4 vendedores. Cada vendedor cuando realiza una 
venta emite una factura. Se desea saber al final del mes:
a)  Cuántas facturas emitió cada vendedor
b)  Total facturado por cada vendedor
Cada factura contiene la siguiente información:
-Nro.  de factura 
-Nro. de vendedor 
-Monto de la factura
Se procesan 500 facturas.
 */
public class ejercicio21
{
    public static void main(String[] args){
     
        int factura []= new int [500];
        
        int vendedor []= new int [factura.length];
        
        float monto []= new float [factura.length];
        
        int ventasuno=0;
        int ventasdos=0;
        int ventastres=0;
        int ventascuatro=0;
        
        float contuno=0;
        float contdos=0;
        float conttres=0;
        float contcuat=0;
        
        for(int i=0; i<factura.length; i++){
         
            factura[i]=i+1;
            vendedor[i]=(int)Math.floor(Math.random()*(4)+1);
            monto[i]=(float)Math.floor(Math.random()*1000);
            
            switch(vendedor[i]){
                
                case 1:
                        ventasuno++;
                        contuno=contuno+monto[i];
                            break;
                            
                case 2:
                        ventasdos++;
                        contdos=contdos+monto[i];
                            break;
                            
                case 3:
                        ventastres++;
                        conttres=conttres+monto[i];
                            break;
                            
                case 4:
                        ventascuatro++;
                        contcuat=contcuat+monto[i];
                            break;
            }
            
            /*System.out.println("+--------------+");
            System.out.println("+--------------+");
            System.out.println("Nro de ticket " + factura[i]);
            System.out.println("Vendedor ID " + vendedor[i]);
            System.out.println("Monto de la compra: $" + monto[i]);
            System.out.println("+--------------+");
            System.out.println("¡Hasta pronto!");*/
        }
        
        System.out.println("Estas son las ventas totales de cada vendedor");
        System.out.println("ID1: " + ventasuno + " ventas, con un monto de $" + contuno);
        System.out.println("ID2: " + ventasdos + " ventas, con un monto de $" + contdos);
        System.out.println("ID3: " + ventastres + " ventas, con un monto de $" + conttres);
        System.out.println("ID4: " + ventascuatro + " ventas, con un monto de $" + contcuat);
    }
}
