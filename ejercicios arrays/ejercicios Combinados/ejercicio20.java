package ejerciciosCombinadoss;

/**
Una editorial de libros posee 12 libros diferentes numerados del 1 al 12.
Cuenta con una planilla en la que figuran los siguientes datos:
-Nro. del libro
-Titulo
-Autor
-Costo
-Precio de venta
-Cantidad de ejemplares vendidos. Se  pide:
a)  Título del libro con mayor cantidad de ejemplares vendidos.
b)  Autor y título del libro con menor costo.
c)  Facturación total de la editorial.
d)  Ganancia de la editorial.
 */
public class ejercicio20
{
    public static void main(String[] args){
     
        int n=12;
        
        int nro []= new int [n];
        String titulo []= new String [n];
        String autor []= new String [n];
        float costo []= new float [n];
        float precioVenta []= new float [n];
        int ejemplaresVendidos []= new int [n];
        
        titulo[0]="El extranjero";
        autor[0]="Albert Camus";
        
        titulo[1]="En busca del tiempo perdido";
        autor[1]="Marcel Proust";
        
        titulo[2]="El proceso";
        autor[2]="Franz Kafka";
        
        titulo[3]="El principito";
        autor[3]="Antoine de Saint-Exupéry";
        
        titulo[4]="La condición humana";
        autor[4]="André Malraux";
        
        titulo[5]="Viaje al fin de la noche";
        autor[5]="Louis-Ferdinand Céline";
        
        titulo[6]="Las uvas de la ira";
        autor[6]="John Steinbeck";
        
        titulo[7]="Por quién doblan las campanas";
        autor[7]="Ernest Hemingway";
        
        titulo[8]="El gran Meaulnes";
        autor[8]="Alain-Fournier";
        
        titulo[9]="La espuma de los días";
        autor[9]="Boris Vian";
        
        titulo[10]="El segundo sexo";
        autor[10]="Simone de Beauvoir";
        
        titulo[11]="El ser y la nada";
        autor[11]="Jean-Paul Sartre";
        
        int masVendido=0;
        String libMasVend="";
        float menCosto=30;
        String autorbarato="";
        String titulobarato="";
        
        float ingreso []= new float [n];
        float ganTotal=0;
        float facTotal=0;
        
        for(int i=0; i<n; i++){
         
            nro[i]=i+1;
            costo[i]=(float)Math.floor(Math.random()*(10+10)+10);
            precioVenta[i]=(float)Math.floor(Math.random()*(100+100)+50);
            ejemplaresVendidos[i]=(int)Math.floor(Math.random()*(45+45)+10);
            
            ingreso[i]=(ejemplaresVendidos[i]*precioVenta[i])-(ejemplaresVendidos[i]*costo[i]);
            
            facTotal=facTotal+(ejemplaresVendidos[i]*precioVenta[i]);
            
            ganTotal=ganTotal+ingreso[i];
            
            if(ejemplaresVendidos[i]>masVendido){
                
                masVendido=ejemplaresVendidos[i];
                libMasVend=titulo[i];
            }
            
            if(costo[i]<menCosto){
                
                menCosto=costo[i];
                autorbarato=autor[i];
                titulobarato=titulo[i];
                
            }
            
            System.out.println(nro[i] + ":" + titulo[i] + " de " + autor[i] + "; " + costo[i] + " de costo; " + precioVenta[i] + " es el precio de venta; se vendieron " + ejemplaresVendidos[i] + " ejemplares.");
        }
        
        System.out.println("El libro más vendido es «" + libMasVend + "»");
        System.out.println("El libro de menor costo es «" + titulobarato + "»" + " del autor " + autorbarato);
        System.out.println("La facturación total de la editorial es de $" + facTotal);
        System.out.println("Su ganancia de $" + ganTotal);
    }
}
