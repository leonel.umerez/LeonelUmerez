package ejerciciosCombinadoss;

/**
 Una empresa editora de libros tiene una tirada mensual de 100 libros. 
 Cada vez que un libro sale a la venta se registra: 
-Nro.   de libro
-Género (codificado del 1 al 10)
-Autor
-Precio
Se pide:
a)  Cantidad de libros por Género.
b)  Recaudación por Género.
c)  Cantidad de libros con precio > 10$
d)  Precio Promedio.
 */
public class ejercicio22
{
    public static void main(String[] args){
        
        int libros []= new int [100];
        int nro []= new int [libros.length];
        int genero []= new int [libros.length];
        String autor []= new String [10];
        float precio []= new float [libros.length];
        int cantlib []= new int [autor.length];
        float reclib []= new float [autor.length];
        
        autor[0]="Truman Capote";
        autor[1]="Charles Bukowski";
        autor[2]="Marguerite Duras";
        autor[3]="Vladimir Nobokov";
        autor[4]="Isak Dinesen";
        autor[5]="Franz Kafka";
        autor[6]="James Joyce";
        autor[7]="Marcel Proust";
        autor[8]="Emile Zola";
        autor[9]="León Tolstói";
        
        int mayor=0;
        float cont=0;
        float prom=0;
        
        for(int i=0; i<libros.length; i++){
            
            nro[i]=i;
            genero[i]=(int)Math.floor(Math.random()*10);
            precio[i]=(float)Math.floor(Math.random()*(2+2)+8);
            
            switch(genero[i]){
                
                case 0:
                    cantlib[0]++;
                    reclib[0]=reclib[0]+precio[i];
                        break;
                case 1:
                    cantlib[1]++;
                    reclib[1]=reclib[1]+precio[i];
                        break;
                case 2:
                    cantlib[2]++;
                    reclib[2]=reclib[2]+precio[i];
                        break;
                case 3:
                    cantlib[3]++;
                    reclib[3]=reclib[3]+precio[i];
                        break;
                case 4:
                    cantlib[4]++;
                    reclib[4]=reclib[4]+precio[i];
                        break;
                case 5:
                    cantlib[5]++;
                    reclib[5]=reclib[5]+precio[i];
                        break;
                case 6:
                    cantlib[6]++;
                    reclib[6]=reclib[6]+precio[i];
                        break;
                case 7:
                    cantlib[7]++;
                    reclib[7]=reclib[7]+precio[i];
                        break;
                case 8:
                    cantlib[8]++;
                    reclib[8]=reclib[8]+precio[i];
                        break;
                case 9:
                    cantlib[9]++;
                    reclib[9]=reclib[9]+precio[i];
                        break;
            }
            
            if(precio[i]>10){
             
                mayor++;
            }
            
            cont=cont+precio[i];
        }
        
        prom=cont/libros.length;
        
        System.out.println("$" + prom + " es el promedio de precios");
        System.out.println(mayor + " libros resultaron más caros de $10");
        
        for(int i=0; i<10; i++){
            
            System.out.println("+--------------+");
            System.out.println("Este es el género " + i + ", el del mismísimo " + autor[i]);
            System.out.println("Se vendieron " + cantlib[i] + " libros");
            System.out.println("Lo recaudado es de $" + reclib[i]);
        }
    }
}
