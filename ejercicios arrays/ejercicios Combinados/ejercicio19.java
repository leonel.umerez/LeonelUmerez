package ejerciciosCombinadoss;

/**
19)  Una empresa de productos alimenticios comercializa 10 productos los cuales 
se identifican por nros. correlativos a partir del 1. 
Se debe realizar  la carga de los precios de dichos artículos y una vez 
efectuada averiguar e imprimir:
a)  Precio máximo y nro. de artículo al que corresponde.
b)  Precio mínimo y nro. de articulo al que corresponde.
c)  Cantidad de artículos con precio superior al precio promedio.
d)  Cantidad de artículos con precio superior a 7$.
 */
public class ejercicio19
{
    public static void main(String[] args){
        
        int n=10;
        
        float precio []= new float [n];
        int producto []= new int [n];
        
        float max=0;
        float min=20;
        int caro=0;
        int barato=0;
        float contador=0;
        float promedio=0;
        int masCaros=0;
        int masDeSiete=0;
        
        for(int i=0; i<n; i++){
            
            precio[i]=(float)(Math.random()*(6+6)+3);
            producto[i]=i+1;
            
            if(precio[i]>max){
             
                max=precio[i];
                caro=producto[i];
            }
            
            if(precio[i]<min){
             
                min=precio[i];
                barato=producto[i];
            }
            
            contador=contador+precio[i];
            
            if(i>n-2){
                
                promedio=contador/n;
             
                for(int j=0; j<n; j++){
                    
                    if(precio[j]>promedio){
                     
                        masCaros++;
                    }
                    
                    if(precio[j]>7){
                        
                        masDeSiete++;
                    }
                    
            }
        }
    }
    
    System.out.println("El precio máximo es de $" + max + " y pertenece al artículo " + caro);
    System.out.println("El precio mínimo es de $" + min + " y pertenece al artículo " + barato);
    System.out.println("Hay " + masCaros + " artículos superiores al promedio de precios");
    System.out.println("Y son " + masDeSiete + " los artículos que cuestan más de $7");
}
}
