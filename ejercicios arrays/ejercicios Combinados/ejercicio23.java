package ejerciciosCombinadoss;

/**
Un banco tiene 10 sucursales. Cada una de ellas mueve cierta cantidad de 
pesos por mes en concepto de depósitos y extracciones. Cada vez que el 
cliente hace un depósito o una extracción queda registrado: 
-Nro. de sucursal 
-Monto 
-Código de Transacción      1  Depósito
2  Transacción
El ingreso de datos finaliza con Nro. de sucursal igual a cero.
Se pide imprimir:
a)  Cuánto recaudó cada sucursal en concepto de depósitos.
b)  Cuánto recaudó cada sucursal en concepto de extracciones.
c)  Cuál es  la sucursal que más recaudó.
d)  Cuál es la sucursal que menos recaudó
e)  Imprimir el nro. de sucursal y recaudación por orden decreciente de recaudación.j
 */
public class ejercicio23
{
    public static void main(String[] args){
        
        int i=0;
        
        int sucursal [] = new int [10];
        int n [] = new int [sucursal.length];

        double recaudac []= new double [sucursal.length];
        double concext []= new double [sucursal.length];
        double concdep []= new double [sucursal.length];
        
        double max=0;
        double min=100000000;
        int sucmas=0;
        int sucmen=0;
        
        for(i=0; i<sucursal.length; i++){
            
            n[i]=(int)Math.floor(Math.random()*50);
            
            double monto [] = new double [n[i]];
            int codigo []= new int [n[i]];   
            int concepto []= new int [n[i]];
            
            for(int j=0; j<n[i]; j++){
                
                monto[j]=(double)Math.floor(Math.random()*20000);
                codigo[j]=j;
                codigo[j]++;
                
                recaudac[i]=recaudac[i]+monto[j];
                
                concepto[j]=(int)Math.floor(Math.random()*2);
                
                if(concepto[j]>0){
                    
                    concdep[i]=concdep[i]+monto[j];
                }else{
                 
                    concext[i]=concext[i]+monto[j];
                }
                
                System.out.println("Sucursal " + i);
                System.out.println("Monto de transacción: $" + monto[j]);
                System.out.println("Serie " + i + (concepto [j]+1) + codigo[j]);
                
            }
            
            if(recaudac[i]>max){
                
                max=recaudac[i];
                sucmas=i;
            }
            
            if(recaudac[i]<min){
             
                min=recaudac[i];
                sucmen=i;
            }
            
            System.out.println("La recaudación de la sucursal " + i + " es de $" + recaudac[i]);
            System.out.println("Esto se entiende por $" + concext[i] + " de ingresos en concepto de extracciones, y " + concdep[i] + " por depósitos.");
                
            double temp=0;
            int controlador=1;
            
            for(int z=0; z<1; z++){
                
                System.out.println("Ordenados los ingresos en modo descendente.");
                
                for(int c=0; c<n[i]; c++){
         
                        for(int y=0; controlador>0; y++){
                
                            controlador=0;
                            
                            for(int v=0; v<n[i]-1; v++){
                                
                                if(monto[v]<monto[v+1]){
                    
                                    temp=monto[v+1];
                                    monto[v+1]=monto[v];
                                    monto[v]=temp;
                
                                        controlador=1;
                                    }  
                                    
                                }
                        }
            
                        System.out.println("$" + monto[c]);
                    }
        
                }
            }
        
        System.out.println("+----------------------------------------------------+");
        System.out.println("+----------------------------------------------------+");      
            
        System.out.println("La sucursal que más recaudó fue la nro " + sucmas + " con un monto de $" + recaudac[sucmas]);
        System.out.println("La sucursal que menos, la nro " + sucmen + " con un monto de $" + recaudac[sucmen]);
        
        System.out.println("+----------------------------------------------------+");
        System.out.println("+----------------------------------------------------+");
    }
    
        
 }

