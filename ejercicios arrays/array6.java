
/**6)   Dadas 25 edades, cargarlas en un vector y caleuier e imprimir:
a.  Edad promedio       
b.  Cantidad de edades mayores que 18 
c.  Imprimir todas las edades que sean mayores a la edad promedio.
 */
public class array6
{
    public static void main(String[] args){
     
        int arr []= new int [25];
        
        int suma = 0;
        int mayor = 0;
        
        System.out.println("Los valores de los elementos que guardan edades mayores de edad son los siguientes: ");
        
        for(int i=0; i<arr.length; i++){
         
            arr[i]=(int)Math.floor(Math.random()*75);
            
            suma = suma+arr[i];
            
            if(arr[i]>17){
             
                mayor++;
                
                System.out.println(arr[i]);
            }
        }
        
        int prom = suma/arr.length;
        
        System.out.println("La edad promedio es de: " + prom + ". Mientras que la cantidad de mayores son: " + mayor);
        
    }
}
