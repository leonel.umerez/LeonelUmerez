/**1)   Cargar un vector de 50 elementos e imprimir:
a.  La  4ta. Componente. 
b.  La  2da. componente.
c.  Las componentes en orden invertida.
d.  El producto entre la primera y la última componente.    
e.  Las componentes de índice par 
f.  Las componentes de índice impar*/

public class array1
{
    public static void main(String[] args){
        
        double arr[]= new double [50];
        
        for(int i=0; i<arr.length; i++){
            
            arr[i]=Math.floor(Math.random()*100);

        }
        
            System.out.println("Imprimir el cuarto componente del array");
        
            System.out.println(arr[3]);
            
            System.out.println("Imprimir el segundo componente del array");
            
            System.out.println(arr[1]);
            
            System.out.println("Imprimir los componentes del array en forma invertida");
            
        for(int i=0; i<arr.length; i++){
            
            System.out.println(arr[(arr.length-1)-i]);
        }
        
            System.out.println("Imprimir el producto entre el primer y el último componente del array");
        
            System.out.println(arr[0]*arr[49]);
            
            System.out.println("Imprimir los componentes de índice par");
            
        for(int i=0; i<arr.length; i+=2){
         
            System.out.println(arr[i]);
            
        }
        
            System.out.println("Imprimir los componentes de índice impar");
        
        for(int i=1; i<arr.length; i+=2){

            System.out.println(arr[i]);
            
        }
    }
}
