
/**
17)    Dados 1os vectores de1 ej. 13, ordenarlos en forma ascendente por sueldo del Empleado.
 */
public class array17
{
    public static void main(String[] args){
        
        int n = 30;
        
        int sueldo []= new int [30];
        
        String nombre []= new String [30];
        
        int controlador=1;
        int temp=0;
        String tempnom="";
        
        int suelmax= 0;
        int emplmax= 0;
        int suelmin= 30000;
        int emplmin= 0;
        
        nombre [0]="González";
        nombre [1]="Rodríguez";
        nombre [2]="Gómez";
        nombre [3]="Fernández";
        nombre [4]="López";
        nombre [5]="Díaz";
        nombre [6]="Martínez";
        nombre [7]="Pérez";
        nombre [8]="García";
        nombre [9]="Sánchez";
        nombre [10]="Romero";
        nombre [11]="Sosa";
        nombre [12]="Álvarez";
        nombre [13]="Torres";
        nombre [14]="Ruiz";
        nombre [15]="Ramírez";
        nombre [16]="Flores";
        nombre [17]="Acosta";
        nombre [18]="Benítez";
        nombre [19]="Medina";
        nombre [20]="Suárez";
        nombre [21]="Herrera";
        nombre [22]="Aguirre";
        nombre [23]="Pereyra";
        nombre [24]="Gutiérrez";
        nombre [25]="Giménez";
        nombre [26]="Molina";
        nombre [27]="Silva";
        nombre [28]="Castro";
        nombre [29]="Rojas";
        
        for(int i=0; i<n; i++){
            
            sueldo[i]=(int)Math.floor(Math.random()*(10000+5000)+10000);
            
            if(i>n-2){
                
                for(int j=0; controlador>0; j++){
                    
                    controlador=0;
                    
                    for(int z=0; z<n-1; z++){
                        
                        if(sueldo[z]>sueldo[z+1]){
                            
                            temp=sueldo[z+1];
                            sueldo[z+1]=sueldo[z];
                            sueldo[z]=temp;
                            
                            tempnom=nombre[z+1];
                            nombre[z+1]=nombre[z];
                            nombre[z]=tempnom;
                            
                            controlador=1;
                        }
                    }
                }
            }
        }
        
        System.out.println("En orden, estos son los nombres con sus sueldos correspondientes:");
        
        for(int i=0; i<n; i++){
            
            System.out.println(nombre[i] + " percibe $" + sueldo[i]);
            
        }
    }
}
