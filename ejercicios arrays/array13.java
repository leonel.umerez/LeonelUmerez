
/**
13) Dado un vector SUELDOS, cargado con los sueldos de 30 empleados y un 
vector NOMBRES asociado al anterior, se pide imprimir:     
a.  Sueldo máximo.  
b.  Nombre del empleado que gana más.       
c.  Nombre del empleado que gana menos.
 */
public class array13
{
    public static void main(String[] args){
        
        int n = 30;
        
        int sueldo []= new int [30];
        
        String nombre []= new String [30];
        
        int suelmax= 0;
        int emplmax= 0;
        int suelmin= 30000;
        int emplmin= 0;
        
        nombre [0]="González";
        nombre [1]="Rodríguez";
        nombre [2]="Gómez";
        nombre [3]="Fernández";
        nombre [4]="López";
        nombre [5]="Díaz";
        nombre [6]="Martínez";
        nombre [7]="Pérez";
        nombre [8]="García";
        nombre [9]="Sánchez";
        nombre [10]="Romero";
        nombre [11]="Sosa";
        nombre [12]="Álvarez";
        nombre [13]="Torres";
        nombre [14]="Ruiz";
        nombre [15]="Ramírez";
        nombre [16]="Flores";
        nombre [17]="Acosta";
        nombre [18]="Benítez";
        nombre [19]="Medina";
        nombre [20]="Suárez";
        nombre [21]="Herrera";
        nombre [22]="Aguirre";
        nombre [23]="Pereyra";
        nombre [24]="Gutiérrez";
        nombre [25]="Giménez";
        nombre [26]="Molina";
        nombre [27]="Silva";
        nombre [28]="Castro";
        nombre [29]="Rojas";
        
        for(int i=0; i<n; i++){
            
            sueldo[i]=(int)Math.floor(Math.random()*(10000+5000)+10000);
            
            if(sueldo[i]>suelmax){
                
                suelmax=sueldo[i];
                emplmax=i;
            }
            
            if(sueldo[i]<suelmin){
                
                suelmin=sueldo[i];
                emplmin=i;
            }
            
            System.out.println(sueldo[i]);
        }
        
        System.out.println("El sueldo máximo es de $" + suelmax);
        System.out.println("El empleado que más cobra es " + nombre[emplmax]);
        System.out.println("El empleado que menos cobra es " + nombre[emplmin]);
    }
}
