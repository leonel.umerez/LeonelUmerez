
/**
10) Una empresa desea procesar sueldos de sus   empleados. Para cada empleado se conoce: SUELDO y NOMBRE. Se desea saber: 
a.  Nombre del o los empleados con sueldo mayor al sueldo promedio.
b.  cantidad de empleados que ganan más de 500
 */
public class array10
{
    public static void main(String[] args){
     
        int n []= new int [10];
        
        String empleado []= new String [n.length];
        int sueldo []= new int [n.length];
        
        empleado[0]="González";
        empleado[1]="Rodríguez";
        empleado[2]="López";
        empleado[3]="Fernández";
        empleado[4]="García";
        empleado[5]="Pérez";
        empleado[6]="Martínez";
        empleado[7]="Gómez";
        empleado[8]="Díaz";
        empleado[9]="Sánchez";
        
        sueldo[0]=750;
        sueldo[1]=1000;
        sueldo[2]=1500;
        sueldo[3]=300;
        sueldo[4]=400;
        sueldo[5]=550;
        sueldo[6]=350;
        sueldo[7]=800;
        sueldo[8]=800;
        sueldo[9]=600;
        
        int sumasueldo=0;
        int masdequin=0;
        
            for(int j=0; j<n.length; j++){
                
                sumasueldo=sumasueldo+sueldo[j];
                
                double sueldoprom=sumasueldo/empleado.length;
                
                if(j>n.length-2){
                 
                    System.out.println("El sueldo promedio es de $" + sueldoprom);
                    
                    System.out.println("Los empleados con mayor sueldo al promedio son los siguientes:");
                    
                    for(int z=0; z<n.length; z++){
                     
                        
                        
                        if(sueldo[z]>sueldoprom){
                            
                            System.out.print(empleado[z] + " ");
                            
                        }
                    }
                }
                
                if(sueldo[j]>500){
                    
                    masdequin++;
                }
                
                
            }

        System.out.println();
        System.out.println(masdequin + " empleados cobran más de $500");
    }
}
