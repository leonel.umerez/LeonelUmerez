Proceso CCB9
	serie <- 1;
	contadorPos <- 0;
	contadorNeg <- 0;
	contadorPos2 <- 0;
	contadorNeg2 <- 0;
	calificacion <- 0;
	cantidad <- 700;
	Escribir '----------------';
	Escribir 'Se calificar� el promedio general del estudiantado en esta universidad a partir de una muestra azarosa de 700 estudiantes';
	Escribir '----------------';
	Escribir 'C�mo par�metro para establecer lo antedicho, se establecer� un umbral con la nota 5 (cinco) debajo de la cual se considerara pobre el rendimiento de los alumnos';
	Escribir '-----------------';
	Escribir "El material se presenta en 3 (tres) planillas habilitadas para nuestros investigadores en el relevamiento de datos.";
	Escribir "-----------------";
	Escribir 'En la planilla ',serie,' encontramos los siguientes resultados';
	Escribir 'Cantidad de alumnos con promedio mayor o igual a 5:';
	Leer contadorPos;
	Mientras contadorPos>cantidad Hacer
		Escribir 'ERROR. Ha ingresado un n�mero mayor al disponible dados los datos anteriores. Reingrese teniendo en cuenta que restan analizar ',cantidad,' alumnos';
		Escribir '-ggg-';
		Escribir 'Cantidad de alumnos con promedio mayor o igual a 5';
		Leer contadorPos;
	FinMientras
	cantidad <- cantidad-contadorPos;
	Escribir '-----------------';
	Escribir 'Mientras que encontramos que los alumnos con un promedio menor son:';
	Leer contadorNeg;
	Mientras contadorNeg>cantidad Hacer
		Escribir 'ERROR. Ha ingresado un n�mero mayor al disponible dados los datos anteriores. Reingrese teniendo en cuenta que restan analizar ',cantidad,' alumnos';
		Escribir '-ggg-';
		Escribir 'Mientras que encontramos que los alumnos con un promedio menor son:';
		Leer contadorNeg;
	FinMientras
	cantidad <- cantidad-contadorNeg;
	Escribir '-----------------';
	Escribir 'En la planilla ',serie,' pudimos evaluar un total de ',700-cantidad,' alumnos, rest�ndonos para la �ltima planilla un total de ',cantidad,' alumnos';
	serie <- serie+1;
	Escribir '-----------------';
	Si cantidad>0 Entonces
		Escribir 'En la planilla ',serie,' encontramos los siguientes resultados';
		Escribir 'Cantidad de alumnos con promedio mayor o igual a 5:';
		Leer contadorPos2;
		Mientras contadorPos2>cantidad Hacer
			Escribir 'ERROR. Ha ingresado un n�mero mayor al disponible dados los datos anteriores. Reingrese teniendo en cuenta que restan analizar ',cantidad,' alumnos';
			Escribir '-ggg-';
			Escribir 'Cantidad de alumnos con promedio mayor o igual a 5';
			Leer contadorPos2;
		FinMientras
		cantidad <- cantidad-contadorPos2;
		Escribir '-----------------';
		Escribir 'Mientras que encontramos que los alumnos con un promedio menor son:';
		Leer contadorNeg2;
		Mientras contadorNeg2>cantidad Hacer
			Escribir 'ERROR. Ha ingresado un n�mero mayor al disponible dados los datos anteriores. Reingrese teniendo en cuenta que restan analizar ',cantidad,' alumnos';
			Escribir '-ggg-';
			Escribir 'Mientras que encontramos que los alumnos con un promedio menor son:';
			Leer contadorNeg2;
		FinMientras
		cantidad <- cantidad-contadorNeg2;
		Escribir '-----------------';
		Escribir 'Analizados los datos de las dos planillas al momento, tenemos que ',contadorPos+contadorPos2,' alumnos tienen un promedio mayor o igual a 5, mientras que ',contadorNeg+contadorNeg2,' alumnos tienen uno menor.';
		Escribir "----------------";
		Si cantidad>0 Entonces
			Escribir 'Los alumnos de la planilla 3 tienen los datos sin relevamientos verificado por el �ndice de error requerido en la investigaci�n, por lo que sus datos no ser�n tenidos en cuenta.';
			Escribir "-------------------";
			Escribir 'A raz�n de remarcar esta situaci�n, en la planilla 3, y SIN CALIFICACION UTIL PARA EL RELEVAMIENTO, encontramos ',cantidad,' alumnos.';
		FinSi
	FinSi
	Escribir "------------------";
	Escribir 'Con los datos relevados, concluimos que la universidad:';
	Escribir "------------------";
	Si contadorNeg+contadorNeg2>=5400 Entonces
		Escribir 'TIENE UN PROMEDIO MUY BAJO EN SUS ALUMNOS';
	SiNo
		Escribir 'PRESENTA DATOS NO DESTACABLES';
	FinSi
FinProceso

