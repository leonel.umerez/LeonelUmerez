Proceso CCB8
	contadorSup = 0;
	contadorPos = 0;
	contadorNeg = 0;
	calificacion <- 0;
	cantidad <- 20;
	Escribir "----------------";
	Escribir "Respecto a la n�mina de 20 alumnos, se imprimir� al final un conteo detallando la cantidad de ellos aprobados, reprobados y promocionados.";
	Escribir "----------------";
	Para cantidad<-1 Hasta cantidad Hacer
		Escribir 'El alumno con el legajo ' cantidad " ha calificado con:";
		Leer calificacion;
		Si calificacion>=7 Entonces
			contadorSup = contadorSup+1;
		SiNo
			Si calificacion>=4 Entonces
				contadorPos = contadorPos+1;
			SiNo
				contadorNeg = contadorNeg+1;
			FinSi
		FinSi
		Escribir "-----------------";
	FinPara
	Escribir "De los " cantidad-1 " alumnos evaluados, " contadorSup " han promocionado, " contadorPos " aprobaron, mientras que " contadorNeg " deber�n recursar la materia.";
	Escribir "-----------------";
FinProceso

