Proceso CCB4
	contadorPos = 0;
	contadorNeg = 0;
	contadorCero = 0;
	num <- 0;
	cantidad <- 10;
	Escribir "----------------";
	Escribir "Debemos identificar cuantos n�meros positivos, negativos, y cuantos ceros se inscriben en los siguientes 10 n�meros";
	Escribir "----------------";
	Para cantidad<-1 Hasta cantidad Hacer
		Escribir 'Ingrese n�mero:';
		Leer num;
		Si num=0 Entonces
			contadorCero = contadorCero+1;
		SiNo
			Si num>0 Entonces
				contadorPos = contadorPos+1;
			SiNo
				contadorNeg = contadorNeg+1;
			FinSi
		FinSi
		Escribir "-----------------";
	FinPara
	Escribir "-----------------";
	Escribir "Se han registrado " contadorPos " n�meros positivos, " contadorNeg " n�meros negativos, y se ha repetido " contadorCero " vez/veces el 0.";
	Escribir "-----------------";
FinProceso

