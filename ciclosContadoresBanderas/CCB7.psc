Proceso CCB7
	contadorPos = 0;
	contadorNeg = 0;
	sueldo <- 0;
	cantidad <- 20;
	Escribir "----------------";
	Escribir "Respecto a la n�mina de 20 empleados, �cu�ntos cobran m�s de $2000 y cu�ntos menos?";
	Escribir "----------------";
	Para cantidad<-1 Hasta cantidad Hacer
		Escribir 'El empleado con el legajo ' cantidad " cobra $:";
		Leer sueldo;
		Si sueldo>2000 Entonces
			contadorPos = contadorPos+1;
		SiNo
			contadorNeg = contadorNeg+1;
		FinSi
		Escribir "-----------------";
	FinPara
	Escribir "De los 20 empleados analizados, " contadorPos " perciben m�s de $2000, mientras que " contadorNeg " igual o menos de esa cifra.";
	Escribir "-----------------";
FinProceso

